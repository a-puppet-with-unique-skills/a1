function sendAjax(obj) {
    // 对传入的参数做判断 - 校验
    // 验证method参数
    if(obj.method === undefined) {
        obj.method = 'get'
    } else {
        // 传递了method - 限制method必须是get/post/put/delete
        if(obj.method.toLowerCase() != 'get' && obj.method.toLowerCase() != 'post' && obj.method.toLowerCase() != 'put' && obj.method.toLowerCase() != 'delete') {
            // 手动抛出错误
            throw new Error('请求方式不正确！')
        }
    }
    // 验证url
    if(obj.url === undefined) {
        throw new Error('请求地址不能为空！')
    } else {
        if(typeof obj.url != 'string') {
            throw new Error('请求地址不正确！')
        }
    }
    // 验证async
    if(obj.async === undefined) {
        obj.async = true
    } else {
        // 传递了async
        if(typeof obj.async != 'boolean') {
            throw new Error('async必须是布尔值！')
        }
    }
    var xhr = new XMLHttpRequest
    xhr.open(obj.method, obj.url, obj.async)
    if(obj.data != undefined) {
        // 设置数据格式
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
        // 对数据做判断如果是对象就转成字符串
        if(typeof obj.data === 'object') {
            var arr = []
            for(var key in obj.data) {
                arr.push(key + '=' + obj.data[key])
            }
            var str = arr.join('&')
            xhr.send(str)
        } else {
            xhr.send(obj.data)
        }
    } else {
        xhr.send()
    }
    // 判断同步异步
    if(obj.async === false) {
        var msg = xhr.responseText
        // console.log(msg);
        obj.success(msg)
    } else {
        xhr.onreadystatechange = function() {
            if(xhr.readyState === 4) {
                if(xhr.status >= 200 && xhr.status < 300) {
                    var msg = xhr.responseText
                    // console.log(msg);
                    obj.success(msg)
                }
            }
        }
    }
}