---
typora-copy-images-to: media
---

# 邻家优选

## 一、项目介绍

本项目是一个优选项目，给邻家超市提供线上销售支持，解决了日用品蔬菜等在传统app上购买配送限制方面的问题，消费者可以通过这款app进行蔬菜粮油，日用百货等的购买，通过vue-cli脚手架搭建vue项目，通过vue-router完成app各页面路由配置，实现首页，分类页面商品详情页面看视频页面，各页面间的相互访问跳转，使用vuex实现数据管理，方便组件之间进行数据共享。

项目技术：vue+router+vuex+axios+swiper+vant+json-server

项目页面如下：

![1670038965700](media/1670038965700.png) 

 ![1670039502640](media/1670039502640.png)

 ![1670039596452](media/1670039596452.png) 

 共分为首页、分类页、视频页、购物车页、登录、注册、我的 7个页面。

数据由json-server启动json文件提供。

## 二、开发环境

- 开发工具：vs code并安装vue开发插件：Vetur
- 开发环境：windows
- 项目运行环境：node v10+
- Vue脚手架： @vue/cli
- 代码版本工具：Git
- 接口工具：json-server

## 三、项目开发

### 1、接口调试

#### 1.1、安装json-server

下载安装：

```shell
npm i json-server -g
```

测试工具：

```shell
json-server --version
```

![1670040040068](media/1670040040068.png) 

#### 1.2、启动接口服务器

找到数据文件data.json，在改文件所在文件夹，执行命令：

```shell
json-server data.json
```

![1670039995788](media/1670039995788.png) 

#### 1.3、测试接口

利用postman或谷歌插件postwomen

获取轮播图：

![1670040209989](media/1670040209989.png) 

获取分类数据：

![1670040247486](media/1670040247486.png) 

添加购物车：

![1670040381404](media/1670040381404.png) 

获取购物车数据：

![1670040419552](media/1670040419552.png) 

注册：



登录：



个人信息：



### 2、项目搭建

#### 2.1、安装工具

安装vue项目脚手架工具：

```shell
npm i @vue/cli -g
```

测试vue脚手架工具：

```shell
vue --version
```

![1670040525354](media/1670040525354.png) 

#### 2.2、创建项目

在指定文件夹中，用脚手架创建项目：

```shell
vue create linjia
```

![1670040693628](media/1670040693628.png) 

![1670040885755](media/1670040885755.png) 

![1670040947970](media/1670040947970.png) 

![1670041013026](media/1670041013026.png) 

![1670041044878](media/1670041044878.png) 

![1670041125532](media/1670041125532.png) 

![1670041189656](media/1670041189656.png) 

![1670041359605](media/1670041359605.png) 

#### 2.3、运行项目

![1670041495091](media/1670041495091.png) 

![1670041622906](media/1670041622906.png) 

打开浏览器输入：http://localhost:8080，运行项目：

![1670041685312](media/1670041685312.png) 

#### 2.4、文件目录介绍

![1670042018408](media/1670042018408.png) 

我们平常开发会在src文件夹中进行：

![1670042297193](media/1670042297193.png) 

### 3、路由配置

#### 3.1、配置路径和组件

指定使用什么路径打开页面，显示哪个组件文件

 ```js
import Vue from 'vue'
import VueRouter from 'vue-router'
// 导入组件文件
import Types from '../views/Types/index.vue'
// vue内置@，表示src文件夹
import Video from '@/views/Video/index.vue'
// 可以省略后缀
import Cart from '@/views/Cart/index'
// 如果要使用文件夹下的index文件，可以省略index
import User from '@/views/User'
import Register from '@/views/User/register'
import Login from '@/views/User/login'
import NotFound from '@/views/NotFound'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    // 跳转到首页路由
    redirect: '/home'
  },
  {
    path: '/home',
    // 指定组件 - 动态加载，按需打包
    component: () => import('../views/Home')
  },
  {
    path: '/types',
    // 指定组件 - 提前打包好
    component: Types
  },
  {
    path: '/video',
    component: Video
  },
  {
    path: '/cart',
    component: Cart
  },
  {
    path: '/user',
    component: User,
    // 注册和登录作为我的页面的子页面
    children: [
      {
        path: '/register',
        component: Register
      },
      {
        path: '/login',
        component: Login
      }
    ]
  },
  // 404路由，当访问的路径不是我们设置好的路径时要显示的组件
  {
    path: '*',
    component: NotFound
  }
]

const router = new VueRouter({
  routes
})

export default router
 ```

#### 3.2、创建组件

![1670048420410](media/1670048420410.png)  

#### 3.3、浏览器访问测试

![1670048441500](media/1670048441500.png) 

![1670048454475](media/1670048454475.png) 

#### 3.4、处理根组件

我们发现每个页面上面的内容是不变的，那是因为所有组件都属于根组件的子组件，父组件的内容，在子组件会固定显示，所以我们需要对根组件处理：

App.vue

```vue
<template>
  <div id="app">
    <router-view/>
  </div>
</template>

<style lang="scss">

</style>
```

这样页面内容就正常了。

### 4、首页开发

首页由顶部logo、搜搜区域、轮播图、推荐分类、推荐商品、底部组成。

#### 4.1、logo部分

将logo图片放在assets文件夹中，创建图片标签，引入logo图片：

```vue
<template>
    <div>
        <div class="logo">
            <img src="../../assets/home-title.png" alt="">
        </div>
    </div>
</template>

<script>

export default {
    data () {
        return {
            
        }
    },
    methods: {
        
    }
}
</script>

<style>
    .logo img{
        width: 100%;
    }
    .logo{
        background-color: #f00;
    }
</style>
```

![1670048924463](media/1670048924463.png) 

#### 4.2、搜索区域

##### 4.2.1、vant介绍

在vue开发中，通常会使用已经有人写好的ui组件库来布局页面，常见的ui组件库有：vant、elementui、antd。。。移动端的布局，我们首选vant组件库。

vant官网：https://vant-contrib.gitee.io/vant/#/zh-CN

vue2版本要配合vant2进行开发，将vant官网切换到版本2：https://vant-contrib.gitee.io/vant/v2/#/zh-CN/

##### 4.2.2、vant使用步骤

- 下载安装vue

  ```shell
  npm i vant@latest-v2
  ```

  

- 配置

  下载安装插件：

  ```shell
  npm i babel-plugin-import
  ```

  将下面配置放在babel.config.js中：

  ```js
  plugins: [
      ['import', {
        libraryName: 'vant',
        libraryDirectory: 'es',
        style: true
      }, 'vant']
    ]
  ```

  如果在文件中原本有配置，增加即可：

  ![1670049321760](media/1670049321760.png) 

- 使用

  - script标签中导入

    ```js
    import Vue from 'vue';
    import { Search } from 'vant';
    
    Vue.use(Search);
    ```

    

  - 模板中复制代码演示：

    ```vue
    <van-search
      v-model="value"
      shape="round"
      background="#4fc08d"
      placeholder="请输入搜索关键词"
    />
    ```

  重启项目看到效果：

  ![1670049859975](media/1670049859975.png) 

  代码：

  ```vue
  <template>
      <div>
          <div class="logo">
              <img src="../../assets/home-title.png" alt="">
          </div>
          <div class="search">
              <van-search
              v-model="value"
              shape="round"
              background="#4fc08d"
              placeholder="请输入搜索关键词"
              />
          </div>
      </div>
  </template>
  
  <script>
  import Vue from 'vue';
  import { Search } from 'vant';
  
  Vue.use(Search);
  export default {
      data () {
          return {
              value: '' // 模板中需要的数据也要定义好
          }
      },
      methods: {
          
      }
  }
  </script>
  
  <style>
      .logo img{
          width: 100%;
      }
      .logo{
          background-color: #f00;
      }
  </style>
  
  ```

#### 4.3、轮播图

轮播图使用swiper制作。

下载安装swiper：

```shell
npm i swiper
```

打开swiper演示网站：https://www.swiper.com.cn/demo/index.html

![1670050031170](media/1670050031170.png) 

![1670050050282](media/1670050050282.png) 

找到对应的我们需要的轮播图，然后在新窗口中打开：

![1670050130458](media/1670050130458.png) 

查看网页源代码，将html部分复制到vue中：

```vue
<div class="swiper mySwiper">
    <div class="swiper-wrapper">
        <div class="swiper-slide">Slide 1</div>
        <div class="swiper-slide">Slide 2</div>
        <div class="swiper-slide">Slide 3</div>
        <div class="swiper-slide">Slide 4</div>
        <div class="swiper-slide">Slide 5</div>
        <div class="swiper-slide">Slide 6</div>
        <div class="swiper-slide">Slide 7</div>
        <div class="swiper-slide">Slide 8</div>
        <div class="swiper-slide">Slide 9</div>
    </div>
    <div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>
    <div class="swiper-pagination"></div>
</div>
```

将css部分复制到vue中：

```css
.swiper {
    width: 100%;
    height: 100%;
}

.swiper-slide {
    text-align: center;
    font-size: 18px;
    background: #fff;

    /* Center slide text vertically */
    display: -webkit-box;
    display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    -webkit-justify-content: center;
    justify-content: center;
    -webkit-box-align: center;
    -ms-flex-align: center;
    -webkit-align-items: center;
    align-items: center;
}

.swiper-slide img {
    display: block;
    width: 100%;
    height: 100%;
    object-fit: cover;
}
```

将js部分放在vue的mounted生命周期中：

```js
var swiper = new Swiper(".mySwiper", {
    cssMode: true,
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
    pagination: {
        el: ".swiper-pagination",
    },
    mousewheel: true,
    keyboard: true,
});
```

需要引入swiper和样式：

```js
import Swiper from 'swiper'
import "swiper/swiper-bundle.min.css"
```

swiper就能用了，如果swiper需要配置，通过查询swiper文档进行查询修改：https://www.swiper.com.cn/api/index.html

此时swiper中轮播图的内容，并不是我们要的banner图片，所以我们需要发送请求获取banner图片，然后将请求回来的图片，作为vue的数据，动态渲染到页面中。

发请求需要使用axios。

下载axios：

```shell
npm i axios
```

导入axios：

```js
import axios from 'axios'
```

使用axios发请求，axios使用方法：https://www.axios-http.cn/docs/api_intro

在created生命周期中发送请求，将请求回来的数据作为实例数据：

```js
data () {
    return {
        value: '',
        banners: []
    }
},
created() {
    axios.get('http://localhost:3000/banner').then(res => {
        this.banners = res.data
    })
}
```

轮播图页面中的结构，通过遍历渲染：

```vue
<div class="banner">
    <div class="swiper mySwiper">
        <div class="swiper-wrapper">
            <div class="swiper-slide" v-for="banner in banners" :key="banner.id">
                <img :src="banner.image" alt="">
            </div>
        </div>
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-pagination"></div>
    </div>
</div>
```

![1670051182058](media/1670051182058.png) 

#### 4.4、推荐分类

发起请求获取分类数据，动态渲染到页面中。

请求：

```js
axios.get('http://localhost:3000/types').then(res => {
    this.types = res.data
})
```

渲染：

```vue
<div class="types">
    <div class="type" v-for="type in types" :key="type.id">
        <img :src="type.icon" alt="">
        <p>{{type.name}}</p>
    </div>
</div>
```

样式：

```css
.types{
    display: flex;
    justify-content: space-between;
    flex-wrap: wrap;
    text-align: center;
    padding: 0 10px;
}
.types img{
    width: 50px;
    height: 50px;
}
.types .type{
    margin: 10px;
}
```

效果：

![1670062112244](media/1670062112244.png) 

#### 4.5、推荐商品

渲染：

```vue
<div class="recommends">
    <div class="recommend" v-for="recommend in types" :key="recommend.id">
        <h3>
            <img src="../../assets/l-icon.png" alt="">
            {{recommend.name}}
            <img src="../../assets/r-icon.png" alt="">
        </h3>
        <div class="product" v-for="product in recommend.products" :key="product.id">
            <p>本商品由{{product.supplier}}提供</p>
            <img :src="product.banner" alt="">
            <p>规格：{{product.specs}}</p>
            <p class="price">价格：￥{{product.price}}元</p>
            <div class="btns">
                <van-button round type="warning">查看详情</van-button>
                <van-button round type="warning">加入购物车</van-button>
            </div>

        </div>
    </div>
</div>
```

vant按钮的使用：

```js
import { Button } from 'vant';

Vue.use(Button)
```

样式：

```css
.recommends{
    padding: 0 10px;
}
.recommends h3{
    text-align: center;
}
.recommends h3 img{
    margin: 0 20px;
    vertical-align: middle;
}
.recommends .recommend{
    text-align: center;
}
.recommends .product img{
    width: 100%;
}
.recommends .product{
    border: 1px solid #ccc;
    border-radius: 10px;
    padding:0 10px 10px;
}
.recommends .product .price{
    font-size: 20px;
    color: #f00;
}
.btns{
    display: flex;
    justify-content: space-around;
}
.btns .van-button{
    width: 30%;
}
```

效果：

![1670071615579](media/1670071615579.png) 

#### 4.6、底部

底部采用vant中的标签栏布局：

vue组件页面：

```vue
<van-tabbar v-model="active">
  <van-tabbar-item icon="home-o">标签</van-tabbar-item>
  <van-tabbar-item icon="search">标签</van-tabbar-item>
  <van-tabbar-item icon="friends-o">标签</van-tabbar-item>
  <van-tabbar-item icon="setting-o">标签</van-tabbar-item>
</van-tabbar>
```

导入vant中的标签栏组件以及设置数据：

```js
import { Tabbar, TabbarItem } from 'vant';
Vue.use(Tabbar);
Vue.use(TabbarItem)

data () {
    return {
        value: '',
        banners: [],
        types: [],
        active: 0
    }
},
```

更改页面内容：

```vue
<div class="footer">
    <van-tabbar v-model="active">
        <van-tabbar-item icon="home-o">首页</van-tabbar-item>
        <van-tabbar-item icon="label-o">分类</van-tabbar-item>
        <van-tabbar-item icon="video-o">视频</van-tabbar-item>
        <van-tabbar-item icon="cart-circle-o">购物车</van-tabbar-item>
        <van-tabbar-item icon="user-o">我的</van-tabbar-item>
    </van-tabbar>
</div>
```

自动会固定在底部：

![1670072243224](media/1670072243224.png) 

### 5、封装底部

底部是每个页面都要使用的，所以我们会将底部作为一个公共组件被其他页面使用。

在components文件夹下新建Footer文件夹，Footer里面再新建index.vue，将底部内容整体剪切到文件中：

![1670072460551](media/1670072460551.png) 

在首页中导入Footer组件使用：

![1670072758293](media/1670072758293.png) 

在模板中，使用组件名称作为标签名渲染：

![1670072789021](media/1670072789021.png) 

效果依旧。

### 6、封装请求

axios在以后每个组件都要使用，且每次响应的结果，都不是最终的数据，需要进行过滤，所以对axios进行封装，方便后续调用。

#### 6.1、封装axios

在src下新建request文件夹，其中新建axios.js，导入axios，并配置axios的请求拦截和响应拦截：

```js
import axios from 'axios'

// 设置所有请求的基准地址
axios.defaults.baseURL = 'http://localhost:3000';

// 添加请求拦截器
axios.interceptors.request.use(function (config) {
    // 在发送请求之前做些什么
    return config;
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
});

// 添加响应拦截器
axios.interceptors.response.use(function (response) {
    // 2xx 范围内的状态码都会触发该函数。
    // 对响应数据做点什么
    return response.data; // 过滤响应数据，我们最后接收到的就是response.data
}, function (error) {
    // 超出 2xx 范围的状态码都会触发该函数。
    // 对响应错误做点什么
    return Promise.reject(error);
});

export default axios
```

#### 6.2、封装请求地址

新建api.js，用于设置请求地址：

```js
export default {
    banner: '/banner',
    types: '/types',
    recommends: '/recommends',
    products: '/products',
    video: '/video',
    cart: '/cart',
    user: '/user'
}
```

#### 6.3、封装请求函数

新建index.js，用于配置所有请求的方法：

```js
import axios from './axios.js'
import obj from './api.js'
const {banner, types, recommends, products, video, cart, user} = obj

export function getBanner() {
    return axios.get(banner)
}

export function getTypes() {
    return axios.get(types)
}

export function getRecommends() {
    return axios.get(types).then(data => {
        return new Promise(resolve => resolve(data.slice(0, 3)))
    })
}

export function getProducts() {
    return axios.get(products)
}

export function getVideo() {
    return axios.get(video)
}

export function addCart(params) {
    return axios.get(cart).then(data => {
        if(data.length) {
            let cartPro = data.find(item => item.id === params.id)
            if(cartPro) {
                return axios.put(cart + '/' + cartPro.id, params)
            }
        }
        return axios.post(cart, params)
    })
}

export function getCartList() {
    return axios.get(cart)
}

export function register(params) {
    return axios.get(user).then(data => {
        if(data.length) {
            let userData = data.find(item => item.username === params.username)
            if(userData) {
                return new Promise(resolve => {
                    resolve({
                        status: 402,
                        msg: '用户名被占用'
                    })
                })
            }
        }
        return axios.post(user, params)
    })
}

export function login(params) {
    return axios.get(user).then(data => {
        if(data.length) {
            let userData = data.find(item => item.username === params.username && item.password === params.password)
            if(userData) {
                return new Promise(resolve => {
                    resolve({
                        status: 200,
                        msg: '登录成功'
                    })
                })
            }
        }
        return new Promise(resolve => {
            resolve({
                status: 401,
                msg: '登录失败'
            })
        })
    })
}

export function getUsers() {
    return axios.get(user)
}
```

#### 6.4、使用封装

在首页导入request下的index.js，将请求的函数导入，并调用得到结果：

```js
import {getBanner, getTypes, getRecommends} from '@/request'

created() {
    getBanner().then(data => this.banners = data)
    // axios.get('http://localhost:3000/banner').then(res => {
    //     this.banners = res.data
    // })
    getTypes().then(data => this.types = data)
    // axios.get('http://localhost:3000/types').then(res => {
    //     this.types = res.data
    // })
    getRecommends().then(data => this.recommends = data)
},
```

效果依旧。

#### 6.5、优化多个请求

我们在首页请求了3次，这3次都是异步请求，每次请求成功都会给数据赋值，每次赋值都会造成页面重新渲染。

简单来说，就是因为这3次请求，页面重新渲染了3次，造成了多余的性能浪费。

为了提高性能，我们希望，这3次请求都完成之后，再渲染。这样页面就只渲染一次了。

使用async/await来解决。将这3次请求都放在一个方法中，在created中调用这个方法即可：

```js
methods: {
    async request() {
        let banners = await getBanner()
        let types = await getTypes()
        let recommends = await getRecommends()
        this.banners = banners
        this.types = types
        this.recommends = recommends
    }
},
created() {
    this.request()
    // getBanner().then(data => this.banners = data)
    // axios.get('http://localhost:3000/banner').then(res => {
    //     this.banners = res.data
    // })
    // getTypes().then(data => this.types = data)
    // axios.get('http://localhost:3000/types').then(res => {
    //     this.types = res.data
    // })
    // getRecommends().then(data => this.recommends = data)
},
```

也可以利用Promise.all来解决。

Promise.all方法，用于同时发送多次请求，当所有请求结束后，一起来处理结果：

```js
request() {
    Promise.all([getBanner(), getTypes(), getRecommends()]).then(arr => {
        // arr是数组，包含3个请求的结果
        [this.banners, this.types, this.recommends] = arr
    })
}
```

### 7、分类页

#### 7.1、底部跳转

点击底部导航，需要能跳转页面，所以给底部导航，添加链接：

```vue
<van-tabbar-item icon="home-o" url="#/home">首页</van-tabbar-item>
<van-tabbar-item icon="label-o" url="#/types">分类</van-tabbar-item>
<van-tabbar-item icon="video-o" url="#/video">视频</van-tabbar-item>
<van-tabbar-item icon="cart-circle-o" url="#/cart">购物车</van-tabbar-item>
<van-tabbar-item icon="user-o" url="#/my">我的</van-tabbar-item>
```

之后点击底部导航可以跳转页面。底部导航栏的样式后面处理。

#### 7.2、顶部搜索

顶部搜索模板代码：

```vue
<div class="headers">
    <van-search v-model="value" placeholder="请输入搜索关键词" />
</div>
```

顶部搜索js代码：

```js
data () {
    return {
        value: '',
    }
},
```

顶部样式：

```css
.header{
    position: fixed;
    left: 0;
    top: 0;
    width: 100%;
    z-index: 2;
}
```



#### 7.3、左侧菜单栏

模板

```vue
<div class="left">
    <van-sidebar v-model="activeKey">
        <van-sidebar-item :title="type.name" v-for="type in types" :key="type.id" />
    </van-sidebar>
</div>
```

js

```js
import Vue from 'vue';
import {getTypes} from '@/request'
import { Sidebar, SidebarItem } from 'vant';
Vue.use(Sidebar);
Vue.use(SidebarItem);

data () {
    return {
        value: '',
        activeKey: 0,
        types: [],
    }
},
created() {
    this.getTypesReq()
},
methods: {
    async getTypesReq() {
        this.types.unshift({
            name: '全部',
            id: +new Date()
        })
        this.types = this.types.concat(await getTypes())
    }
}
```

左侧样式：

```css
.left{
    width: 20%;
    float: left;
}
.left .van-sidebar{
    position: fixed;
    left: 0;
    top: 50px;
}
```



#### 7.4、右侧商品

因为内容较多，使用vant提供的list组件实现懒加载。

模板：

```vue
<div class="right">
    <van-list
              v-model="loading"
              :finished="finished"
              finished-text="没有更多了"
              @load="onLoad"
              >
        <div class="product" v-for="product in products" :key="product.id">
            <img :src="product.banner" alt="">
            <p>规格：{{product.specs}}</p>
            <p class="price">价格：￥{{product.price}}元</p>
            <div class="btns">
                <van-button round type="warning">加入购物车</van-button>
            </div>
        </div>
    </van-list>
</div>
```

js：

```js
import Vue from 'vue';
import {getTypes, getProducts} from '@/request'
import { List } from 'vant';
import { Button } from 'vant';

Vue.use(Button)
Vue.use(List);

data () {
    return {
        value: '',
        activeKey: 0,
        types: [],
        products: [],
        loading: false,
        finished: false,
        page: 1
    }
},
created() {
    this.getTypesReq()
    this.request()
},
methods: {
    async getTypesReq() {
        this.types.unshift({
            name: '全部',
            id: +new Date()
        })
        this.types = this.types.concat(await getTypes())
    },
    request(type = '全部') {
        getProducts(type).then(data => {
            console.log(data);
            let pros = data.slice((this.page-1)*5, this.page*5)
            if(pros.length < 5) {
                this.finished = true
            } else {
                this.page++
            }
            this.loading = false;
            this.products = this.products.concat(pros)
            console.log(this.products);
        })
    },
    onLoad() {
        this.request()
    }
}
```

css：

```css
.right{
    width: 80%;
    float: right;
    padding: 50px 0;
}
.product{
    padding: 10px;
    border: 1px solid #ccc;
    margin: 10px;
    border-radius: 10px;
}
.product .van-button{
    display: block;
    width: 50%;
}
.product img{
    width: 100%;
    border-radius: 10px;
}
.price{
    font-size: 20px;
    color: #f00;
}
```

#### 7.5、左侧菜单点击

给左侧菜单添加点击事件：

```vue
<div class="left">
    <van-sidebar @change="typeChange" v-model="activeKey">
        <van-sidebar-item :title="type.name" v-for="type in types" :key="type.id" />
    </van-sidebar>
</div>
```

定义方法处理事件，并修改请求方法：

```js
data () {
    return {
        value: '',
        activeKey: 0,
        types: [],
        products: [],
        loading: false,
        finished: false,
        page: 1,
        type: '全部'
    }
},
    
request(type) {
    getProducts(type).then(data => {
        let pros = data.slice((this.page-1)*5, this.page*5)
        if(pros.length < 5) {
            this.finished = true
        } else {
            this.page++
        }
        this.loading = false;
        this.products = this.products.concat(pros)
    })

},

onLoad() {
    this.request(this.type)
},
typeChange(index) {
    this.products = []
    this.page = 1
    this.type = this.types[index].name
    this.request(this.type)
}
```

#### 7.6、添加购物车

给按钮绑定单击事件：

```vue
<van-button @click="addcart(product)" round type="warning">加入购物车</van-button>
```

添加对应方法：

```js
addcart(product) {
    // let username = getCookie('username')
    // if(!username) {
    //     // 页面跳转
    //     this.$router.push({path: "/login", query: {url: '#/types'}});
    //     return
    // }
    // product.username = username
    product.number = 1
    addCart(product).then(data => {
        Toast.success('添加成功') // vant中的提示
    })
}
```

### 8、视频页

vue模板：

```vue

```

js：

```js

```







